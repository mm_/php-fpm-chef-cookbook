#
# Cookbook Name:: php-fpm
# Attributes:: default
#
# Author:: Adam Jacob (<adam@opscode.com>)
# Author:: Joshua Timberman (<joshua@opscode.com>)
#
# Copyright 2009-2011, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# In order to update the version, the checksum attribute should be
# changed too. It is in the source.rb file, though we recommend
# overriding attributes by modifying a role, or the node itself.
# default['php-fpm']['source']['checksum']
default['php-fpm']['version'] = "1.2.6"
default['php-fpm']['package_name'] = "php-fpm"
default['php-fpm']['dir'] = "/etc/php-fpm"
default['php-fpm']['daemon_dir'] = "/etc/php-fpm.d/"
default['php-fpm']['log_dir'] = "/var/log/php-fpm"
default['php-fpm']['binary'] = "/usr/sbin/php-fpm"

case node['platform']
when "debian","ubuntu"
  default['php-fpm']['user']       = "www-data"
  default['php-fpm']['init_style'] = "runit"
when "redhat","centos","scientific","amazon","oracle","fedora"
  default['php-fpm']['user']       = "php-fpm"
  default['php-fpm']['init_style'] = "init"
  default['php-fpm']['repo_source'] = "epel"
else
  default['php-fpm']['user']       = "www-data"
  default['php-fpm']['init_style'] = "init"
end

default['php-fpm']['group'] = node['php-fpm']['user']
default['php-fpm']['listen'] = {
	"path" => "127.0.0.1:9000"
}

default['php-fpm']['max_children'] = 50
default['php-fpm']['start_servers'] = 5
default['php-fpm']['spare_servers'] = 5
default['php-fpm']['min_spare_servers'] = 5
default['php-fpm']['max_spare_servers'] = 35
default['php-fpm']['max_requests'] = 500
default['php-fpm']['slowlog'] = "/var/log/php-fpm/www-slow.log"
default['php-fpm']['admin_flag_log_errors'] = 'off'
